# Sound Engineering 2

## Course Content
The first part of the course deals with the theory and practice of microphone types and working principles as well as grounding and interfacing practice. Applications of microphones are studied in stereo microphone recording techniques and in sound reinforcement situations. As a preparation for the second part of the course, an intensive frequency hearing training is performed. The second part of the course deals with the basic principles of mixing and balancing where the relationship between music and sound is studied in detail. This part of the course is organized in intensive hands-on sessions. The group will be split up in smaller groups of 2 students.

## Objectives
At the end of this course, you:
- are able to independently design a simple microphone setup, including positioning and focusing. This both for recording and amplification, bearing in mind the musical material and the acoustical and architectural properties of the surroundings;
- are able to independently recognise frequency ranges and formant areas to an accuracy of ± 1 octave, expressed in Hertz (Hz).
- are able to independently decide on mix questions during a multitrack mixing process, based on the relation between sound and the musical material in question.

Professor: [Paul Jeukendrup](https://www.berlinerfestspiele.de/en/artist/df9f1bad-06be-4e6d-9fd3-26f911b33eaf/Paul-Jeukendrup)

## Examination
Test 1: A written test at the end of the first part of the course, on 14.02.2024 involving both theory questions and cases (numeric result). Test 2: Participation in the intensive mix classes (Pass/Fail). Both tests have to be concluded with a positive result in order to pass the course.

## Line Arrays
- **Theory**: Originated from Harry Olson in the 1950s. Used in music for over 20 years, and in venues like churches, train stations, and airports.
- **Design**: J-letter-shaped set of speakers. Multiple loudspeakers create one cohesive sound. Best for specific situations. 
- **Comparison with Point Source Loudspeaker**: Point source loudspeaker usually sounds better but line arrays adapt to equal energy of the dispersion angle.
- **Point Source**: Radiates sound spherically in all directions. Theoretical model with 0 dimensions.
    - **Spherical Wavefront**
    - **Surface Area**
    - **Sound Loss**: Loses 6 dB with double the distance, contrary to the theoretical model.

## Sound Propagation Models
- **Plane Source (November 8)**: Planar wavefront, one direction. Real-world approximation: navy tube (-6dB). No decrease in SPL with distance.
- **Line Source (November 8)**: Cylindrical wavefront, similar to line arrays in reality. Real-world approximation: highway. Formula for intensity (I) and perimeter (2πr).
    - **Formula for Intensity (I) and Perimeter (2πr)**: `I = P / 2πr` Useful formula for line sources, a type of sound propagation model. It describes how the intensity of sound (I) from a line source decreases with the distance (r) from the source. In the context of line arrays, this  helps in understanding how sound intensity behaves in a cylindrical wavefront
    - **Inverse Square Law for Sound Intensity**: `I ∝ 1/r²` This law applies to point sources of sound. It states that the intensity of sound decreases in proportion to the square of the distance from the source. E.g. individual loudspeakers or non-line array configurations

## Line Arrays in Real-World Applications
- **Characteristics**: Suitable for long distances, large horizontal and small vertical dispersion angles. Doesn't lose much energy vertically.
- **Directionality and Wavelength**: Exploring the relationship between the shape of the line array, directionality, and wavelength.
- **Coupling and Interference**: Discussing the effect of distance between sources on the sound field.

## Air Attenuation Factors
- Influenced by temperature, pressure, and relative humidity.
- Wavelength calculation formula: λ=343 / f.

## Special Characteristics of Line Arrays
- **Modal Equal Energy Curves**: To fit listening space, reach long distances, and avoid reflections.
- **Software Tools**: Maya sound software for acoustic prediction, Array Processing by d&b audiotechnik.

## Case Study: Stockhausen's Gruppen
- **Event**: Musikfest Berlin 2008, Berlin Philharmonic, conducted by Simon Rattle and Daniel. 
- **Setup**: 109 musicians, 3 orchestras, 2 conductors, seating arrangements with 3 stages around the audience.
- **Acoustic Design**: Partitioning wall with diffusers, LF absorbers, and line array systems.

## Sound System Design Techniques and Criteria
- **Multichannel Sound**: Differentiating between systems carrying the same signal and immersive audio systems.
- **Uniform Reinforcement Design**: Principles by Jim Brown and Bob McCarthy for achieving uniform sound pressure levels, full coverage, and minimizing level and special variety.
- **Solution for Uniform SPL**: Shaping equal level contours to fit audience seating, minimizing level variance, and considering the high-frequency roll-off due to air.
- **Loudspeaker Selection and Positioning**: Use of prediction software for array positioning, measurement mics placement, and compensation strategies.

## Multiple Loudspeaker Systems
- **Spatial and Spectral Subsystems**: Choosing and aiming loudspeakers based on the spatial subdivision and audience experience.
- **Criteria for Loudspeaker Selection**: Covering the largest audience area with a minimum number of speakers to reduce ripple.

## Main and Subsystems
- **Main System**: Covers the largest part of the audience. Full range for the principal part of the spectrum.
- **Subsystems**: Extend the main system's spectrum, especially in the low-frequency range.

## Aiming Loudspeakers
- **Strategy**: Considerations for aiming based on audience position and room acoustics.
- **Main Systems Configurations**: Various setups like C (center), LR (left-right), LCR, Multi, Upper/Lower, and Overhead.
- **Sound Pressure Levels**: Balancing between localisation and intelligibility, aiming strategies, and reflection considerations.

## Amplification of Acoustic Music
- **Reasons for Amplification**: Artistic effects, sound quality, acoustical properties, and volume requirements.
- **Historical Overview**: Influences from Schönberg, Pierre Boulez, Moderna, Stockhausen, and Pavarotti's outdoor performances.

## General Design Techniques
- **Amplitude, Frequency Response, and Directionality**: Ensuring full coverage, intelligibility, and even frequency response.
- **Comb Filtering**: Managing the relationship between time difference and frequency.
- **Line Array Usage**: Ideal for large spaces, directional control, and avoiding reflective surfaces.

## Multiple Loudspeaker Systems and Aiming Techniques
- **Proximity Ratio**: As a criteria for loudspeaker placement.
- **Vowel Frequency Recognition**: Understanding how vowel sounds correspond to resonances in the system/space, with a frequency table for reference.

# Exam

## Conceptual Sound Design
- **Connection of Technological Solutions to Musical Material**: Understanding how technology choices impact the interpretation and presentation of musical material.

## Loudspeaker Properties
- **Understanding Loudspeaker Properties**: Comprehending the key characteristics of loudspeakers and their implications for practical applications.

## Multiple Loudspeaker Systems
- **Principles of Multiple Loudspeakers**: Grasping the concepts behind using multiple loudspeakers in a composed system and their practical applications.

## Line Sources
- **Application of Line and Point Sources**: Recognizing the practical uses of line and point sources and being able to justify choices for these systems based on musical requirements.

## Design Techniques
- **Criteria for Evaluating Sound System Design**: Knowledge of the criteria used to assess a sound system design and the ability to evaluate a system against these criteria.

## Multichannel Sound
- **Advantages and Limitations of Multichannel Systems**: Understanding the benefits and constraints of multichannel sound systems.

## Multiple Loudspeaker System Design
- **Spatial and Spectral Subdivision Principles**: Comprehension of the principles of spatial and spectral subdivision in loudspeaker systems, including the ability to distinguish between main and subsystem categories and apply them appropriately.
- **Resonance Frequency Identification**: Skill in determining resonance frequencies by ear, with a resolution of 1 octave.

## Software
- [Spot software](https://www.spotsoftware.nl/)

## Education
- [FOSS Sound Design Next Generation Brochure](https://fossnextgeneration.com/wp-content/uploads/2021/07/foss_sound_design_brochure_2021.pdf)
